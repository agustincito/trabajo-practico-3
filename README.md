﻿
# Métodos computacionales para ingeniería I
Gallardo Agustín - UNComa (2020)

## Comandos de Linux Shell
El siguiente repositorio tendrá una pequeña compilación de algunas de las funciones que se pueden realizar utilizando la linea de comandos LNX Shell. Algunos de los comandos que se explicarán son; como cambiar el nombre de varios archivos de un mismo formato, la creación de scripts para realizar esos cambios, como mover archivos entre carpetas y la creación de una carpeta comprimida para la creación de backups.

Los datos se obtuvieron del siguiente dataset:

(Real and Fake Face Detection. Computational Intelligence and Photography Lab Department of Computer Science, Yonsei University)[https://www.kaggle.com/ciplab/real-and-fake-face-detection/data]