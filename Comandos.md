# Comandos

## Cambio de nombre de varios archivos de un mismo formato

Para cambiar el nombre de varios archivos sin tener que hacerlo uno por uno, podemos generar un script para primero agregar la parte del nombre que queremos modificar, y luego cortar la parte antigua del nombre.
Desde la carpeta donde queremos cambiar los archivos, en este caso ***fake***, abrimos gitBush y escribimos el siguiente c�digo:

>$ for n in *.jpg
> do
> echo mv $n img_$n >> scriptCambioNombre3.sh
> done

Esto lo que har� es renombrar agregando ***img_*** adelante de todos los archivos .jpg que existan en la carpeta. Tambi�n crear� un script que realizar� esa acci�n cuando se lo ejecute. Por ejemplo, para una imagen nombrada ***mid_436_1111*** devolver�:
> mv mid_436_1111 img_mid_436_1111

Luego utilizamos el siguiente comando
> $ cut -d "_" -f 1,2,3,5,6 scriptCambioNombre3.sh > NuevoCambioFinal.sh

Esto significa que teniendo en cuenta el resultado arrojado por el script anterior, y utilizando el cr�terio de ***"_"***, cortar� en 6 segmentos a
> mv mid_436_1111 img_mid_436_1111

al escribir ***-f 1,2,3,5,6*** solo mantendremos la parte que nos interesa, es decir, que el nombre de la foto solo tenga el prefijo ***img_*** y no el ***mid_***, ya que cortaremos eso �ltimo. Esta nueva acci�n la mandamos a un nuevo script, teniendo en cuenta el anterior creado. El resultado final para todas las fotos deber�a ser el siguiente:
> mv mid_436_1111 img_436_1111

## Generar un archivo de salida que nos muestre el cambio realizado
Si queremos realizar un peque�o archivo que nos muestre el cambio realizado, lo que debemos hacer es usar el �ltimo script creado (en este caso*NuevoCambioFinal.sh*) y escribir lo siguiente:
> cut -d " " -f 2,3 --output-delimiter=$'>' NuevoCambioFinal.sh > renameFake.dat

El ***cut -d " "*** nos cortar� el resultado del script en 3 partes, utilizando como criterio el espacio en blanco, con ***-f 2,3*** nos ubicaremos entre ***mid_436_1111 img_436_1111***  y con ***--output-delimiter=$'>'*** lograremos que entre ambos nombres nos escriba un signo ">" que nos mostrar� cu�l es el nombre antiguo del archivo. En la parte de ***NuevoCambioFinal.sh > renameFake.dat***  usamos el �ltimo script creado y lo mandamos a un archivo funcional ***.dat***. El resultado final deber�a arrojar lo siguiente:
> mv mid_436_1111>img_436_1111

Ahora realizaremos los mismos pasos pero para renombrar las fotos de una carpeta llamada ***real*** , reemplazando el sufijo ***real_*** por ***img_***. Tambi�n crearemos un archivo ***.dat*** que nos muestre el nombre anterior y nuevo.

- Primera Parte

Inicializamos gitBush en la carpeta ***real***
Escribimos el siguiente comando para crear un script
>$ for n in *.jpg
> do
> echo mv $n img_$n >> scriptCambioNombreImg.sh
> done

eso nos devuelve
>  mv real_00070 img_real_00070

escribiendo lo siguiente
>$ cut -d "_" -f 1,2,4 scriptCambioNombreImg.sh > NuevoCambioImgReal.sh

deber�a cortar la parte ***real_*** y guardar un nuevo script utilizando el anterior. El resultado final ser�
> mv real_00070 img_00070

- Segunda Parte

Utilizando el nuevo script ***NuevoCambioImgReal.sh*** escribimos el siguiente c�digo:
>$ cut -d " " -f 2,3 --output-delimiter=$'>' NuevoCambioImgReal.sh > renameReal.dat

esto deber�a arrojar
> mv real_00070>img_00070

## Movimiento de archivos entre carpetas
Crearemos una carpeta llamada ***dataSet*** en el mismo directorio donde se encuentran creadas ***fake*** y ***real***, de tal modo que nos queden tres carpetas en un mismo sitio, en este ejemplo, las tres carpetas est�n dentro de otra denominada ***tp3-data***
>fake
>real
>dataSet

Si queremos mover todos los archivos .**jpg** y .**dat** que se encuentren tanto en ***fake*** como ***real*** debemos inicializar git Bush ac� e insertar los siguientes comandos
>$ mv fake/*.jpg dataSet

>$ mv fake/*.dat dataSet

>$ mv real/*.jpg dataSet

>$ mv real/*.dat dataSet

La primera parte ("mv fake/*.jpg" ) nos indica donde se encuentran los archivos que queremos mover y que tipo de archivo son, mientras que ("dataSet") la carpeta donde ser�n movidos.

## Comprensi�n de archivos
Por �ltimo, como tenemos todos los archivos modificados y creados en una carpeta, podemos comprimirla para hacer un backup de toda la informaci�n almacenada en ella. 
Para ello, necesitamos inicializar gitBush en donde est� guardada dicha carpeta y utilizar el comando ***tar***
>$ tar -cvf 16072020dataSet.tar dataSet

donde:
- -c crea el archivo .tar
- -v muestra la descripci�n detallada del proceso de compresi�n
- -f es el nombre del archivo

en este caso la decidimos llamar por la fecha de creaci�n y el nombre de la carpeta ***16072020dataSet***. Luego del nombre debemos explicitar que carpeta queremos es la que queremos comprimir, en este caso, ***dataSet***.